<?php
/**
 * Call actions after configuration is saved
 */
class Magentothem_Themeoptions_Model_Observer
{
	private $dirPath;
    private $filePath;
    private $store_cf;
    private $dir_store;
	
	private function getConfig($cf){
		if(Mage::getStoreConfig('themeoptions/themeoptions_config/'.$cf,$this->store_cf))
			return Mage::getStoreConfig('themeoptions/themeoptions_config/'.$cf,$this->store_cf);
		else
			return Mage::getStoreConfig('themeoptions/themeoptions_config/'.$cf);
	}
	
	private function setLocation () {
		$this->store_cf = Mage::getModel("core/store")->load(Mage::app()->getRequest()->getParam('store'));
		if(Mage::getStoreConfig('design/package/name',$this->store_cf))
			Mage::getStoreConfig('design/package/name',$this->store_cf);
		else
			$this->dir_store = 'happystore/';
		if(Mage::getStoreConfig('design/theme/default',$this->store_cf))
			$this->dir_store .= Mage::getStoreConfig('design/theme/default',$this->store_cf);
		else
			$this->dir_store .= 'default';
        $this->dirPath = Mage::getBaseDir('skin') . '/frontend/'.$this->dir_store.'/css/';
        $this->filePath = $this->dirPath . 'skin.css';
    }
	
	/**
     * After any system config is saved
     */
	public function cssgenerate()
	{
		$section = Mage::app()->getRequest()->getParam('section');
		if ($section == 'themeoptions')
		{
			$this->setLocation();
			$css = '.block .block-title strong,.block-layered-nav #search_pr input,.banner-static .banner-box h2,.ma-newproductslider-container  .ma-newproductslider-title h2,.title_subscribe>h1 {font-family:'.str_replace("+"," ",$this->getConfig('font')).';font-weight:'.$this->getConfig('font_weight').'}';
			$css .= '.timer-grid .box-time-date span,.timer-list .box-time-date span,.price-box .price,.box-timer .box-time-date span,.timer-view .box-time-date span{font-family:'.str_replace("+"," ",$this->getConfig('font_title2')).';font-weight:'.$this->getConfig('font_title2weight').'}';
			$css .= 'body,.breadcrumbs li,.breadcrumbs li a,.toolbar,.toolbar label,.sorter .view-mode .list, .sorter .view-mode .grid,.top-link ,.drop-lang .drop-trigger span,.drop-lang .drop-trigger .sub-lang,.drop-currency .currency-trigger span,.drop-currency .currency-trigger .sub-currency,.pt_custommenu div.pt_menu .parentMenu a,.pt_custommenu div.pt_menu .parentMenu span.block-title,.block-layered-nav h2,.block-layered-nav .content-shopby ol li, .block-layered-nav .content-shopby ol li a,.timer-grid .box-time-date,.timer-list .box-time-date,.product-tabs a,.product-name a,.block-progress .block-title strong,.banner-static .banner-box p,.menu-recent .item-inner .time,.box-timer .box-time-date,.timer-view .box-time-date{font-family:'.str_replace("+"," ",$this->getConfig('font_content')).';font-weight:'.$this->getConfig('font_contentweight');
			$css .='}';
			$css .= '.title-group h2,.form-list label,.page-title h1,.page-title h2,.block-tags .block-content li a,.block-subscribe .subscribe-title h3,.product-view .product-shop .short-description,.cart .page-title h1,.cart .discount h2,.cart .shipping h2,.footer-static .footer-title h2,.footer-static .footer-content ul li a,.footer address,.ma-desc,.categorytabslider .tab_categorys li,.tweet-title h3{font-family:'.str_replace("+"," ",$this->getConfig('font_content2')).';font-weight:'.$this->getConfig('font_content2weight').'}';
			try{
				$fh = new Varien_Io_File(); 
				$fh->setAllowCreateFolders(true); 
				$fh->open(array('path' => $this->dirPath));
				$fh->streamOpen($this->filePath, 'w+'); 
				$fh->streamLock(true); 
				$fh->streamWrite($css); 
				$fh->streamUnlock(); 
				$fh->streamClose(); 
			}
			catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('ThemeOptionsBlacknwhite')->__('Failed creation custom css rules. '.$e->getMessage()));
			}
			
		}
	}
}
