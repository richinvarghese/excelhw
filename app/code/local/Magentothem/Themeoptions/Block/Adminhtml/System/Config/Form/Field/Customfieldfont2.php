<?php
class Magentothem_Themeoptions_Block_Adminhtml_System_Config_Form_Field_Customfieldfont2 extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $output = parent::_getElementHtml($element);

        $output .= '<span id="themeoptions_font2_view" style="font-size:28px;line-height: 28px; display:block; padding:6px 0 0 0">Preview Font</span>
        
		<script type="text/javascript">
            jQuery.noConflict();
            jQuery(function(){
                fontSelect=jQuery("#themeoptions_themeoptions_config_font_title2");
                font2Update=function(){
                    curFont=jQuery("#themeoptions_themeoptions_config_font_title2").val();
					curFont2_text=curFont.replace(/\+/g, " ");
                    jQuery("#themeoptions_font2_view").css({ fontFamily: curFont2_text });
                    jQuery("<link />",{href:"http://fonts.googleapis.com/css?family="+curFont,rel:"stylesheet",type:"text/css"}).appendTo("head");
                }
                fontSelect.change(function(){
                    font2Update();
                }).keyup(function(){
                    font2Update();
                }).keydown(function(){
                    font2Update();
                });
                jQuery("#themeoptions_themeoptions_config_font_title2").trigger("change");
            })
		</script>
        ';
        return $output;
    }
}