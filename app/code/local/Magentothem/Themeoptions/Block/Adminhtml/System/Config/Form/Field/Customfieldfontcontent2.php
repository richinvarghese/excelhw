<?php
class Magentothem_Themeoptions_Block_Adminhtml_System_Config_Form_Field_Customfieldfontcontent2 extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $output = parent::_getElementHtml($element);

        $output .= '<span id="themeoptions_font_content_view2" style="font-size:28px;line-height: 28px; display:block; padding:6px 0 0 0">Preview Font</span>
		<script type="text/javascript">
            jQuery(function(){
                fontSelect=jQuery("#themeoptions_themeoptions_config_font_content2");
                fontcontentUpdate2=function(){
                    curFontcontent=jQuery("#themeoptions_themeoptions_config_font_content2").val();
					curFontcontent_text2=curFontcontent.replace(/\+/g, " ");
                    jQuery("#themeoptions_font_content_view2").css({ fontFamily: curFontcontent_text2 });
                    jQuery("<link />",{href:"http://fonts.googleapis.com/css?family="+curFontcontent,rel:"stylesheet",type:"text/css"}).appendTo("head");
                }
                fontSelect.change(function(){
                    fontcontentUpdate2();
                }).keyup(function(){
                    fontcontentUpdate2();
                }).keydown(function(){
                    fontcontentUpdate2();
                });
                jQuery("#themeoptions_themeoptions_config_font_content2").trigger("change");
            })
		</script>
        ';
        return $output;
    }
}