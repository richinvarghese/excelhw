<?php
class Magentothem_Producttabs_Block_Producttabs extends Mage_Core_Block_Template
{

    public function getProducttabsCfg($cfg)
    {
        return Mage::helper('producttabs')->getProducttabsCfg($cfg);
    }

    public function getProductCfg($cfg)
    {
        return Mage::helper('producttabs')->getProductCfg($cfg);
    }

    public function getTypeDefault()
    {
        $cfg   = $this->getProductCfg('product_type');
        $cfg   = explode(',', $cfg);
        return $cfg[0];
    }

    public function sortTabs()
    {
        return $this->getProducttabsCfg('sort_name');
    }

	public function getTabs()
	{
        $types = Mage::getSingleton("producttabs/system_config_type")->toOptionArray();
        $cfg = $this->getProductCfg('product_type');
        $cfg = explode(',', $cfg);
        $tabs = array();
        foreach ($types as $type) {
            if(in_array($type['value'], $cfg)){
                $tabs[$type['value']] = $type['label'];
            }
        }

        return $tabs;
	}

}